document.addEventListener('DOMContentLoaded', function(){ 
   var req = response();
   console.log(JSON.parse(req.responseText));
}, false);

function response()
{
	const req = new XMLHttpRequest();
	req.addEventListener('load',()=>{
	    if(req.status===200){
	        listarRegistros(JSON.parse(req.responseText));
	        listarRegistros(req.responseText)
	        datos = req.responseText;
	    }else{
	        listarRegistros([req.status, req.statusText]);
	    }
	});

	req.addEventListener('error',()=>{
	    listarRegistros('Error de red');
	});
	req.open('GET', 'http://dummy.restapiexample.com/api/v1/employees',true);
	req.send(null);

	return req;
}

function listarRegistros(text){
    	var nTamanioJson = text.data.length;
    	var table = document.getElementById("tbody");
			table.innerHTML = "";

		for (var i = 0; i < nTamanioJson; i++) {
			//console.log(text.data[i].id);
			//console.log(text.data[i]);
			var row = table.insertRow(0);
	
			var cell1 = row.insertCell(0);
			var cell2 = row.insertCell(1);
			var cell3 = row.insertCell(2);
	
			cell1.innerHTML = text.data[i].employee_name;
			cell2.innerHTML = text.data[i].employee_salary;
			cell3.innerHTML = text.data[i].employee_age;
		};

		
		
}

function buscarEmpleado(){
	clearTable();
	//retornamos una promesa del fetch para poder usarla fuera de este
	var sParametro = document.getElementById('buscador').value;
	return new Promise((resolve, reject) => {
	fetch('http://dummy.restapiexample.com/api/v1/employee/'+sParametro)
		.then(function(respose){
			return respose.text();
			})
			.then(function(data){
				let datos = data;
				resolve(datos);
			});
		});
}

function buscarEmpleadoNombre(){
	clearTable();
	//retornamos una promesa del fetch para poder usarla fuera de este
	return new Promise((resolve, reject) => {
	fetch('http://dummy.restapiexample.com/api/v1/employees')
		.then(function(respose){
			return respose.text();
			})
			.then(function(data){
				let datos = data;
				resolve(datos);
			});
		});
}

class MiBotonBuscar extends HTMLElement{
    constructor(){
        super();
		this.addEventListener('click', function(e){
			buscarEmpleadoNombre().then(data => {
				//var aDatos = JSON.parse(data);
				//var oValues = Object.values(aDatos);

				/*
				var table = document.getElementById("tbody");
				var row = table.insertRow(0); //Se inserta registro buscado en posición 1 (fila 2), la 0 es encabezado
	
				var cell1 = row.insertCell(0);
				var cell2 = row.insertCell(1);
				var cell3 = row.insertCell(2);
		
				cell1.innerHTML = oValues['1'].employee_name;
				cell2.innerHTML = oValues['1'].employee_salary;
				cell3.innerHTML = oValues['1'].employee_age;*/

				console.log(JSON.parse(data));
				var aDatos = JSON.parse(data);

				var sBusqueda = document.getElementById('buscador').value;

				let filtro = aDatos.data.filter((empleado)=>{
					if (empleado.employee_name.includes(sBusqueda)) {
						//alert('existe');
						console.log(empleado);


						var table = document.getElementById("tbody");
						var row = table.insertRow(0); //Se inserta registro buscado en posición 1 (fila 2), la 0 es encabezado
			
						var cell1 = row.insertCell(0);
						var cell2 = row.insertCell(1);
						var cell3 = row.insertCell(2);
				
						cell1.innerHTML = empleado.employee_name;
						cell2.innerHTML = empleado.employee_salary;
						cell3.innerHTML = empleado.employee_age;
					}else{
						//alert('no existe');
					}
				});



			});
        });
	}
}

customElements.define('mi-boton', MiBotonBuscar);
class MiBotonClear extends HTMLButtonElement{
    constructor(){
        super();
        this.addEventListener('click', (e)=>{
        	var sBusqueda = document.getElementById('buscador').value = '';
			response();
        });
    }
	
	static get ceName(){
		return 'mi-boton-clear'
	}
	get is(){
		return this.getAttribute('is');
	}
	set is (value){
		this.setAttribute('is', value || this.ceName);
	}
}

customElements.define('mi-boton-clear', MiBotonClear,{extends:'button'});
class MiInput extends HTMLInputElement{
	constructor() {
		super();
		this.addEventListener('click',function(e){
			
			});
	}
}

customElements.define('mi-input-extendido', MiInput,{extends:'input'});
function enPantalla(text){
       
    let element = document.getElementById('table');
}


function clearTable()
{
 var tableRef = document.getElementById('tbody');
 while ( tableRef.rows.length > 0 )
 {
  tableRef.deleteRow(0);
 }
}


